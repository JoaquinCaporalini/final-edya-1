#include <stdlib.h>
#include <string.h>

#include "interprete.h"

int main(int argc, char** argv) {
  if (argc == 1)
    interprete(0);
  else if (argc == 2 && strcmp("-i", argv[1]) == 0)
    interprete(1);
  return 0;
}
