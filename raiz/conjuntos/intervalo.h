#ifndef __INTERVALO_H__
#define __INTERVALO_H__

#include <stddef.h>

typedef struct _Intervalo {
  int inicial, final;
} *Intervalo;

/*Crea un intervalo*/
Intervalo intervalo_crear(int inicial, int final);

/*Verifica que dos intervalos contengan interseccion por derecha*/
int intervalo_union_contigua_der(Intervalo intervalo1, Intervalo intervalo2);

/*Libera un intervalo*/
void intervalo_destruir(Intervalo intervalo);

/*Copia independiente de un intervalo*/
Intervalo intervalo_copia(Intervalo dato);

/*Interseccion de dos intervalo*/
Intervalo intervalo_interseccion(Intervalo intervalo1, Intervalo intervalo2);

/*Dar precedencia entre dos intervalos (EXTREMOS IZQ)*/
int intervalo_inicial_menor(void *dato1, void *dato2);

/*Dar precedencia entre dos intervalos (EXTREMOS DER)*/
int intervalo_final_mayor(void *dato1, void *dato2);

/*Imprime por consola un intervalo*/
void imprimir_intervalo(void *dato);

#endif                          /* __INTERVALO_H__ */
