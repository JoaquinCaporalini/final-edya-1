#include <stdio.h>
#include <stdlib.h>

#include "gslista.h"
#include "ordenar.h"

//Función auxiliar a Merge Sort
//Recibe los nodos cabeza de dos GLists
//Fusiona dichas listas de acuerdo al criterio de ordenación 
//indicado en la función f
GSNodo *merge(GSNodo * nodo1, GSNodo * nodo2, Compara f) {
  if (!nodo1)
    return nodo2;
  if (!nodo2)
    return nodo1;
  if (f(nodo1->dato, nodo2->dato) < 0) {
    nodo1->sig = merge(nodo1->sig, nodo2, f);
    return nodo1;
  } else {
    nodo2->sig = merge(nodo1, nodo2->sig, f);
    return nodo2;
  }
}

//Función auxiliar a Merge Sort
//devuelve el nodo que se encuentra en la mitad de una lista
//empleando el algoritmo del puntero lento-puntero rápido
GSNodo *split(GSNodo * primero) {
  GSNodo *slow = primero;
  GSNodo *fast = primero;
  while (fast->sig && fast->sig->sig) {
    fast = fast->sig->sig;
    slow = slow->sig;
  }
  GSNodo *temp = slow->sig;
  slow->sig = NULL;
  return temp;
}

//Función que realiza el procedimiento de dividir y fusionar las listas
//A partir del nodo cabeza, pero que no actualiza la lista final
GSNodo *algoritmo_merge_sort(GSNodo * primero, Compara f) {
  if ((!primero) || (!primero->sig)) {
    return primero;
  }
  GSNodo *second = split(primero);
  primero = algoritmo_merge_sort(primero, f);
  second = algoritmo_merge_sort(second, f);

  return merge(primero, second, f);
}

//Algoritmo Merge Sort aplicado a GList
GSList merge_sort(GSList glista, Compara f) {
  glista->primero = algoritmo_merge_sort(glista->primero, f);
  GSNodo *nodo = glista->primero;
  while (nodo && nodo->sig != NULL) {
    nodo = nodo->sig;
  }
  glista->ultimo = nodo;
  return glista;
}
