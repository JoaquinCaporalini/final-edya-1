#ifndef __ORDENAR_H__
#define __ORDENAR_H__

#include <stddef.h>

typedef int (*Compara) (void *dato1, void *dato2);

GSList merge_sort(GSList glista, Compara f);

#endif
