#ifndef __CONJUNTOS_H__
#define __CONJUNTOS_H__

#include <stddef.h>

//Los conjuntos son listas que cumplen una serie de propiedades espeiales:
// -Todos los elementos estan ordenados/
// -No hay interseccion entre los intervalos que se contengan en el conjunto

 /*ACLARACION*/
//Ademas hay que aclarar que se trabajara con el consepto de frozen-sets
//Esto quiere decir que una vez que se crea un conjunto no se le pueden agregar o quitar 
//elementos, a esepcion que se hagan con las operaciones.
typedef GSList Conjunto;

 /**/
//int orden (Intervalo dato1, Intervalo dato2);
/*Crea el espacio y la estructura en memoria*/
    Conjunto conjunto_crear();

/*Verifica si el conjunto NO contiene elementos*/
int conjunto_vacio(Conjunto conjunto);

/*Libera la memoria ocupada por el conjunto*/
void conjunto_destruir(Conjunto conjunto);

/*Transforma una lista de intervalos en un conjunto*/
Conjunto conjunto_simplificar(GSList lista);

/*Dada una lista de intervalos la transforma en un conjunto*/
Conjunto conjunto_inicializar(GSList lista);

/*Realiza la copia de un conjunto, se pierde la relacion entre ellas*/
Conjunto conjunto_copia(Conjunto conjunto);

/*Realiza la operacion de conjuntos union*/
Conjunto conjunto_union(Conjunto conjunto1, Conjunto conjunto2);

/*Realiza la operacion de conjuntos interseccion*/
Conjunto conjunto_inteseccion(Conjunto conjunto1, Conjunto conjunto2);

/*Realiza la operacion del complemento de un conjuntos*/
Conjunto conjunto_complemento(Conjunto conjunto);

/*realiza la diferencia entre dos conjuntos*/
Conjunto conjunto_resta(Conjunto conjunto1, Conjunto conjunto2);

/*Recorre todo el conjunto*/ 
void conjunto_imprimir(Conjunto conjunto);

#endif                          /* __CONJUNTOS_H__ */
