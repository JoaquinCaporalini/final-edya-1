#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "gslista.h"
#include "intervalo.h"
#include "conjunto.h"
#include "ordenar.h"

//Funcion para liberar
void destruir(void *dato) {
  intervalo_destruir(dato);
}

//Trasladar codigo GSLista a Conjuntos
Conjunto conjunto_crear() {
  return gslist_crear();
}

//Trasladar codigo de GSLista a Conjuntos
int conjunto_vacio(Conjunto conjunto) {
  return gslist_vacia(conjunto);
}

//Destrulle la estructura
//Libera la memoria.
void conjunto_destruir(Conjunto conjunto) {
  gslist_destruir(conjunto, destruir);
}

//Simplificar une los intervalos contiguos y elimina los repetidos.
Conjunto conjunto_simplificar(GSList lista) {
  if (gslist_vacia(lista))
    return (Conjunto) lista;
  GSNodo *verificado = lista->primero;
  GSNodo *nodo;
  Intervalo intervaloVer, intervaloNodo;//Es auxiliar para simplificar
  while (verificado && verificado->sig) {
    nodo = verificado->sig;
    intervaloVer = (Intervalo) verificado->dato;
    intervaloNodo = (Intervalo) nodo->dato;
    if (intervalo_union_contigua_der(intervaloVer, intervaloNodo)) {
      ((Intervalo) verificado->dato)->final =
          (intervaloVer->final < intervaloNodo->final) ?
          intervaloNodo->final : intervaloVer->final;
      //Liberar el dato.
      intervalo_destruir(nodo->dato);
      //Desligar nodo.
      verificado->sig = nodo->sig;
      free(nodo);
    } else
      verificado = verificado->sig;
  }
  if (!(verificado->sig))
    lista->ultimo = verificado;
  //No es necesario realizar una simplificacion
  //o ya fueron hechas
  return (Conjunto) lista;
}

//Dada un GSList donde los datos sean intervalos validos
//se adecua al consepto de conjunto.
Conjunto conjunto_inicializar(GSList lista) {
  //Se ordenan con el objetivo de cumplir con las props. de conjuntos
  lista = merge_sort(lista, intervalo_inicial_menor);
  return conjunto_simplificar(lista);
}

//Genera una copia independiente al conjunto dado
Conjunto conjunto_copia(Conjunto conjunto) {
  GSList copia = gslist_crear();
  GSNodo *nodo = conjunto->primero;
  while (nodo) {
    gslist_agregar_final(copia, intervalo_copia(nodo->dato));
    nodo = nodo->sig;
  }
  return (Conjunto) copia;
}

//Dados cos conjuntos se genera la union.
//Generando un nuevo conjunto, independiente a los otros.
Conjunto conjunto_union(Conjunto conjunto1, Conjunto conjunto2) {
  GSList lista = gslist_crear();
  GSNodo *elementos1 = conjunto1->primero;
  GSNodo *elementos2 = conjunto2->primero;
  //Funcion de merge, combina los dos conuntos
  while (elementos1 || elementos2) {
    //Si ambos conjuntos son no vacias
    if (elementos1 && elementos2) {
      //si el primer elemento del primer conjunto es menor
      if (intervalo_inicial_menor(elementos1->dato, elementos2->dato) < 0) {
        gslist_agregar_final(lista, intervalo_copia(elementos1->dato));
        elementos1 = elementos1->sig;
        //printf("& elem1\n");
      }
      //si el primer elemento del segundo conjunto es menor
      else if (intervalo_inicial_menor(elementos2->dato, elementos1->dato) < 0) {
        gslist_agregar_final(lista,
                             intervalo_copia((Intervalo) elementos2->dato));
        elementos2 = elementos2->sig;
      }
      //si ambos primeros son iguales
      //se eligira el mas amplio
      else {
        intervalo_final_mayor(elementos1->dato, elementos2->dato) ?
            gslist_agregar_final(lista, intervalo_copia(elementos1->dato)) :
            gslist_agregar_final(lista, intervalo_copia(elementos2->dato));
        elementos1 = elementos1->sig;
        elementos2 = elementos2->sig;
      }
    }
    //si el primero es no vacio y el segundo es vacio
    else if (elementos1 && !elementos2) {
      gslist_agregar_final(lista, intervalo_copia(elementos1->dato));
      elementos1 = elementos1->sig;
    }
    //si el primero es vacio y el segundo es no vacio
    else if (!elementos1 && elementos2) {
      gslist_agregar_final(lista, intervalo_copia(elementos2->dato));
      elementos2 = elementos2->sig;
    }
  }
  //Se eliminan partes repetidas y se enlasa lo posible.
  return conjunto_simplificar(lista);
}

//Dados dos conjuntos se genera la interseccion de estos.
//Generando un nuevo conjunto, independiente a los otros.
Conjunto conjunto_inteseccion(Conjunto conjunto1, Conjunto conjunto2) {
  GSList lista = gslist_crear();
  //Casos particulares
  //Si uno de los dos conjuntos es vacio, la interseccion es vacia.
  if (conjunto_vacio(conjunto1) || conjunto_vacio(conjunto2))
    return (Conjunto) lista;
  //Si el conjunto1 es el universal
  if (((Intervalo) conjunto1->primero->dato)->inicial == INT_MIN &&
      ((Intervalo) conjunto1->primero->dato)->final == INT_MAX)
    return conjunto_copia(conjunto2);
  //Si el conjunto2 es el universal
  if (((Intervalo) conjunto2->primero->dato)->inicial == INT_MIN &&
      ((Intervalo) conjunto2->primero->dato)->final == INT_MAX)
    return conjunto_copia(conjunto1);
  //Caso General
  GSNodo *elementos1 = conjunto1->primero;
  GSNodo *elementos2 = conjunto2->primero;
  Intervalo interseccion;
  while (elementos1 && elementos2) {
    //Se genera la interseccion de los intervalos de los elementos.
    interseccion = intervalo_interseccion
        ((Intervalo) elementos1->dato, (Intervalo) elementos2->dato);
    //Si la intersecion que se genera
    if (interseccion)
      gslist_agregar_final(lista, interseccion);
    if (intervalo_final_mayor(elementos1->dato, elementos2->dato))
      elementos2 = elementos2->sig;
    else
      elementos1 = elementos1->sig;
  }
  //Se eliminan partes repetidas y se enlasa lo posible.
  return conjunto_simplificar(lista);
}

//Dado un conjunto se le crea su complemento, tomando el universal INT.
//Generando un nuevo conjunto, independiente a los otros.
Conjunto conjunto_complemento(Conjunto conjunto) {
  GSList complemento = gslist_crear();
  //Casos especiales
  //Complemento del congunto vacio
  if (gslist_vacia(conjunto)) {
    gslist_agregar_inicio(complemento, intervalo_crear(INT_MIN, INT_MAX));
    return (Conjunto) complemento;
  }
  Intervalo intervaloTemp = (Intervalo) conjunto->ultimo->dato;
  //Complemento del universal
  if (intervaloTemp->inicial == INT_MIN && intervaloTemp->final == INT_MAX)
    return (Conjunto) complemento;
  //Caso generales
  GSNodo *nodo = conjunto->primero;
  //Indica el principio y el final del complemento
  int inicial = INT_MIN, final = INT_MAX;
  if (intervaloTemp->final == final)
    final = intervaloTemp->inicial;
  intervaloTemp = (Intervalo) nodo->dato;
  if (intervaloTemp->inicial == inicial) {
    inicial = intervaloTemp->final + 1;
    nodo = nodo->sig;
  }
  while (nodo) {
    gslist_agregar_final
        (complemento, intervalo_crear(inicial, intervaloTemp->inicial - 1));
    if (nodo->sig) {
      inicial = intervaloTemp->final + 1;
      intervaloTemp = (Intervalo) nodo->sig->dato;
    }
    nodo = nodo->sig;
  }
  //Colocar el final
  intervaloTemp = (Intervalo) conjunto->ultimo->dato;
  if (final == INT_MAX)
    gslist_agregar_final
        (complemento, intervalo_crear(intervaloTemp->final + 1, final));
  return (Conjunto) complemento;
}

//Dados dos conjuntos se realiza la resta de los dos.
//Generando un nuevo conjunto, independiente a los otros.
Conjunto conjunto_resta(Conjunto conjunto1, Conjunto conjunto2) {
  Conjunto complemento = conjunto_complemento(conjunto2);
  Conjunto salida = conjunto_inteseccion(conjunto1, complemento);
  conjunto_destruir(complemento);
  return salida;
}

//Imprime el contenido de un conjunto.
void conjunto_imprimir(Conjunto conjunto) {
  gslist_recorrer(conjunto, imprimir_intervalo);
  //printf("\n");
}
