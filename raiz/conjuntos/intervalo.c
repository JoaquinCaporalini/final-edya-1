#include "intervalo.h"
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

Intervalo intervalo_crear(int inicial, int final) {
  if (inicial > final)
    return NULL;
  Intervalo intervalo = malloc(sizeof(struct _Intervalo));
  intervalo->inicial = inicial;
  intervalo->final = final;
  return intervalo;
}

//Esta funcion es necesaria para unir dos conjuntos contiguos
int intervalo_union_contigua_der(Intervalo intervalo1, Intervalo intervalo2) {
  if (intervalo1->final != INT_MAX)
    return ((intervalo1->final + 1) >= intervalo2->inicial);
  return 1;
}

void intervalo_destruir(Intervalo intervalo) {
  free(intervalo);
}

Intervalo intervalo_copia(Intervalo dato) {
  Intervalo copia = malloc(sizeof(struct _Intervalo));
  copia->inicial = dato->inicial;
  copia->final = dato->final;
  return copia;
}

Intervalo intervalo_interseccion(Intervalo intervalo1, Intervalo intervalo2) {
  if (intervalo1->final < intervalo2->inicial
      || intervalo1->inicial > intervalo2->final)
    return NULL;
  //Se elije el elemento inicial de la interseccion
  Intervalo interseccion = malloc(sizeof(struct _Intervalo));
  interseccion->inicial = intervalo1->inicial < intervalo2->inicial ?
      intervalo2->inicial : intervalo1->inicial;
  //Se elije el elemento final de la interseccion
  interseccion->final = intervalo1->final < intervalo2->final ?
      intervalo1->final : intervalo2->final;
  return interseccion;
}

//Compara los valores iniciales de dos intervalos
//Para dar una precedencia entre dos intervalos
int intervalo_inicial_menor(void *dato1, void *dato2) {
  Intervalo intervalo1 = (Intervalo) dato1;
  Intervalo intervalo2 = (Intervalo) dato2;
  if (intervalo2->inicial < intervalo1->inicial)
    return 1;
  if (intervalo2->inicial == intervalo1->inicial)
    return 0;
  if (intervalo1->inicial < intervalo2->inicial)
    return -1;
  return -1;
}

//Compara los valores finales de dos intervalos
//Para dar una precedencia entre dos intervalos
int intervalo_final_mayor(void *dato1, void *dato2) {
  Intervalo intervalo1 = (Intervalo) dato1;
  Intervalo intervalo2 = (Intervalo) dato2;
  return intervalo1->final > intervalo2->final;
}


//imprimir un intervalo
//Si los extremos son iguales es porque es un solo numero
//Si los extremos son contiguos es mismo que tener un numero y su sig.
//un intervalo como lo entendemos.
void imprimir_intervalo(void *dato) {
  Intervalo intevalo = dato;
  if (intevalo->inicial == intevalo->final)
    printf("%d", intevalo->final);
  else if (intevalo->inicial + 1 == intevalo->final)
    printf("%d, %d", intevalo->inicial, intevalo->final);
  else
    printf("%d : %d", intevalo->inicial, intevalo->final);
}
