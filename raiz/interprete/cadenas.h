#ifndef __CADENAS_H__
#define __CADENAS_H__

#include <stddef.h>

typedef char *String;

/*Crea el espacio en memoria para esta estructura*/
String string_crear();

/*Libera el espacio que ocupa esta estructua*/
void string_destruir(String string);

/*Da un tamanio de manera inicial al string*/
String string_tamanio(unsigned int largo);

/*Redimenciona el espacio, lo dupica*/
String string_redimencionar(String string, unsigned int *largo);

/*Capturar una linea*/
String get_line(unsigned int *espacios, unsigned int *largo);

/*Imprime todo el contenido de un archivo*/
void imprime_contenido(char * archivoNombre);

#endif                          /* __CADENAS_H__ */
