#ifndef __BUFFER_H__
#define __BUFFER_H__

#include <stddef.h>

#include "cadenas.h"
#include "gslista.h"

enum comandos {
  def_extencion = 1,
  def_comprension,
  op_union,
  op_interseccion,
  op_resta,
  op_complemento,
  com_imprimir,
  com_salir
};

enum errores {
  def_limIntervalo = 1,
  def_intervalo,
  def_variables,
  def_espaciosCont,
  op_noValida,
  com_invalido,
  com_desconocido,
  com_faltaArg,
  com_sobranArg,
  com_empezaAalfa
};

typedef struct _Buffer {
  int estado;
  String conjunto1;
  String conjunto2;
  String conjunto3;
  GSList protoconjunto;
} *Buffer;

/*Crear buffer*/
Buffer crear_buffer();

/*Libera toda la memoria que se pueda haber pedido*/
void memoria_buffer(Buffer buffer);

/*Limpiar buffer para la siguiente iteracion*/
void limpiar_buffer(Buffer buffer);


#endif /*__BUFFER_H__*/
