#ifndef __LECTURA_H__
#define __LECTURA_H__

#include <stddef.h>

/*imprime los elementos de un conjunto*/
int lectura_imprimir(Buffer buffer, unsigned int *espacios, String str,
                     unsigned int largo);

/*complemento de un conjunto*/
//ALIAS = ~ALIAS
int lectura_complemento(Buffer buffer, unsigned int *espacios, String str,
                        unsigned int largo);

/*Reconocimiento de operacion*/
int reconocer_operacion(char op);

/*Ralizar operaciones entre conjuntos*/
//ALIAS = ALIAS # ALIAS
int lectura_operaciones(Buffer buffer, unsigned int *espacios, String str,
                        unsigned int largo);

/*Creacion de conjuntos por extencion*/
//ALIAS = {}
//ALIAS = {a,b,c,d,e,f,g,h,i,j,k,l,...,z}
int lectura_extencion(Buffer buffer, unsigned int *espacios, String str,
                      unsigned int largo);

/*Creacion de conjuntos por comprension*/
//ALIAS = {x : m <= x <= n}
int lectura_comprension(Buffer buffer, unsigned int *espacios, String str);


#endif /*__LECTURA_H__*/
