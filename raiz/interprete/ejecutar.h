#ifndef __EJECUTAR_H__
#define __EJECUTAR_H__

#include <stddef.h>

#include "buffer.h"
#include "tablahash.h"
#include "conjunto.h"

typedef Conjunto(*Operacion) (Conjunto conjunto1, Conjunto conjunto2);

/*Extencion*/
void ejecutar_agregar_conjunto(TablaHash tablaHash, Buffer buffer);

/*complemento*/
void ejecutar_complemento(TablaHash tablaHash, Buffer buffer);

/*operaciones*/
void ejecutar_operacion(TablaHash tablaHash, Buffer buffer, Operacion funcion);

/*imprimir*/
void ejecutar_imprimir(TablaHash tablaHash, Buffer buffer, int bandera);

#endif /*__EJECUTAR_H__*/
