#include <stdlib.h>
#include <stdio.h>
#include "cadenas.h"

void string_destruir(String string) {
  free(string);
}

/*Da un tamanio de manera inicial al string*/
String string_crear(unsigned int largo) {
  return (String) malloc(sizeof(char) * largo);
}

/*Redimenciona el espacio*/
String string_redimencionar(String string, unsigned int *largo) {
  int nuevaMemoria = *largo;
  nuevaMemoria = nuevaMemoria + (nuevaMemoria / 2);
  *largo = nuevaMemoria;
  return (String) realloc(string, sizeof(char) * nuevaMemoria);
}

//Logra tomar de la entrada estandar la linea completa,
//ademas tomando datos necesarios para mas tarde
String get_line(unsigned int *espacios, unsigned int *dimencion) {
  char caracter;
  unsigned largo = 250;
  String entrada = string_crear(largo);

  unsigned int i;
  int j = 0;
  for (i = 0;
       (caracter = getchar()) != EOF && caracter != '\n' && caracter != '\0';
       i++) {

    if (largo == i)
      string_redimencionar(entrada, &largo);
    entrada[i] = caracter;

    if (caracter == ' ' && j <= 9) {
      espacios[j] = i;
      j++;
    }

  }
  entrada = realloc(entrada, sizeof(char) * (i + 1));
  entrada[i] = '\0';
  *dimencion = i;
  return entrada;
}

//Con esta funcion podremos imprimir las instrucciones para
//el correcto uso del interprete
void imprime_contenido(char * archivoNombre){
  FILE *archivo;
 	
  char caracteres[100];
 	
  archivo = fopen(archivoNombre,"r");
 	
  if (archivo == NULL)
    printf("error\n");
  else {
    while (feof(archivo) == 0) {
      fgets(caracteres,100,archivo);
      printf("%s",caracteres);
    }
    fclose(archivo);
  }
  printf("\n");
}