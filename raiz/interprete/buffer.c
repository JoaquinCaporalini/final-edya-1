#include <stdio.h>
#include <stdlib.h>

#include "buffer.h"
#include "cadenas.h"
#include "gslista.h"
#include "intervalo.h"

void raiz(void *a) {
  intervalo_destruir(a);
}

Buffer crear_buffer() {
  Buffer buffer = malloc(sizeof(struct _Buffer));
  buffer->conjunto1 = NULL;
  buffer->conjunto2 = NULL;
  buffer->conjunto3 = NULL;
  buffer->estado = 0;
  buffer->protoconjunto = NULL;
  limpiar_buffer(buffer);
  return buffer;
}

/*Si la operacion no requirio el dato se liberan sus espacios*/
void memoria_buffer(Buffer buffer) {
  //Conjunto 1
  if (buffer->conjunto1)
    string_destruir(buffer->conjunto1);
  buffer->conjunto1 = NULL;
  //Conjunto 2
  if (buffer->conjunto2)
    string_destruir(buffer->conjunto2);
  buffer->conjunto2 = NULL;
  //Conjunto 3
  if (buffer->conjunto3)
    string_destruir(buffer->conjunto3);
  buffer->conjunto3 = NULL;
}

/*Limpiar buffer para la siguiente iteracion*/
void limpiar_buffer(Buffer buffer) {
  //limpia conjuntos
  memoria_buffer(buffer);
  //Reinicia los estados
  buffer->estado = 0;
  buffer->protoconjunto = NULL;
}
