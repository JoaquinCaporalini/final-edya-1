/*Librerias estandares necesarias*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <ctype.h>
#include <math.h>
/*Codigo para el interprete*/
#include "interprete.h"
#include "buffer.h"
#include "cadenas.h"
#include "lectura.h"
#include "ejecutar.h"
/*codigo para conjuntos*/
#include "gslista.h"
#include "intervalo.h"
#include "conjunto.h"
/*Memoria*/
#include "tablahash.h"

//Confirma que lo que hay entre dos espacios sea un signo igual(=)
int confirmar_sig_igual(unsigned int *espacios, String str) {
  return (espacios[1] - espacios[0] - 1) == 1 && str[espacios[0] + 1] == '=';
}

//Verifica que no existan dos espacios contiguos
int espacios_contiguos(unsigned int *espacios) {
  int salida = 0;
  for (int i = 0; i < 8 && espacios[i] != 0 && !salida; i++) {
    if (espacios[i + 1] - espacios[i] == 1)
      salida = 1;
  }
  return salida;
}

//Comoleta el bufer de entrada del programa
void completar_bufer(Buffer buffer) {
  unsigned int largo;
  unsigned int espacios[9] = { 0, 0, 0, 0, 0, 0, 0, 0 };
  String str = get_line(espacios, &largo);
  //printf("%s : %u\n", str, largo);
  if (str[0] == '\0' || largo == 0)
    buffer->estado = 0;         //No hacer nada
  else if (espacios[8] != 0)
    buffer->estado = -com_desconocido;  //Demaciados espacios
  else if (espacios_contiguos(espacios))
    buffer->estado = -def_espaciosCont; //Espacios contiguos
  else if (!isalpha(str[0]))
    buffer->estado = -com_faltaArg;     //El primer caracter no es una letra
  else if (espacios[0] == 0) {
    if (largo == 5 && strcmp("salir", str) == 0)
      buffer->estado = com_salir;       //salir del interprete
    else
      buffer->estado = -com_desconocido;        //Los comandos no empizan con espacios
  } else if (espacios[1] == 0) {
    if (!lectura_imprimir(buffer, espacios, str, largo))
      buffer->estado = (buffer->estado == 0) ? -com_desconocido : buffer->estado;       //Problema
  } else if (espacios[2] == 0) {
    if (confirmar_sig_igual(espacios, str)) {
      if (!lectura_complemento(buffer, espacios, str, largo))
        if (!lectura_extencion(buffer, espacios, str, largo))
          buffer->estado = (buffer->estado == 0) ? -com_desconocido : buffer->estado;   //Problema
    } else
      buffer->estado = -com_invalido;   //Formato incompatible
  } else if (espacios[3] == 0)
    buffer->estado = -1;        //Comando desconocido
  else if (espacios[4] == 0) {
    if (confirmar_sig_igual(espacios, str))
      if (!lectura_operaciones(buffer, espacios, str, largo))
        buffer->estado = (buffer->estado == 0) ? -com_desconocido : buffer->estado;     //desconocido 
  } else if (espacios[5] == 0)
    buffer->estado = -com_desconocido;  //Comando desconocido
  else if (espacios[6] == 0)
    buffer->estado = -com_desconocido;  //Comando desconocido
  else if (espacios[7] == 0)
    buffer->estado = -com_desconocido;  //Comando desconocido
  else if (espacios[8] == 0) {
    if (confirmar_sig_igual(espacios, str))
      if (!lectura_comprension(buffer, espacios, str))
        buffer->estado = 
          (buffer->estado == 0) ? - com_desconocido: buffer->estado; //Problema
  } else {
    buffer->estado = -com_desconocido;  //Tenemos un problema
    //printf("Hola\n");
  }
  free(str);
}

/*Ejecutar comandos*/
void ejecutar_buffer(Buffer buffer, TablaHash almacen, int bandera) {
  //printf("Comando: %d\n", buffer->estado);
  switch (buffer->estado) {
  case (def_extencion):
    ejecutar_agregar_conjunto(almacen, buffer);
    break;
  case (def_comprension):
    ejecutar_agregar_conjunto(almacen, buffer);
    break;
  case (op_union):
    ejecutar_operacion(almacen, buffer, conjunto_union);
    break;
  case (op_interseccion):
    ejecutar_operacion(almacen, buffer, conjunto_inteseccion);
    break;
  case (op_resta):
    ejecutar_operacion(almacen, buffer, conjunto_resta);
    break;
  case (op_complemento):
    ejecutar_complemento(almacen, buffer);
    break;
  case (com_imprimir):
    ejecutar_imprimir(almacen, buffer, bandera);
    break;
  case (com_salir):
    printf("Terminando el interprete\n");
    break;
  default:
    printf("Comando inesperado\n");
    break;
  }
}

/*Ejecutar comandos*/
void ejecutar_errores(Buffer buffer) {
  //printf("error: %d\n", - buffer->estado);
  switch (-buffer->estado) {
  case (def_limIntervalo):
    printf
        ("no se puede definir el conjunto, algun numero exede el limite int\n");
    break;
  case (def_intervalo):
    printf("Definicion de un intervalo no valida\n");
    break;
  case (def_variables):
    printf("las varibles no coinciden para la definicion\n");
    break;
  case (def_espaciosCont):
    printf("En la entrada hay dos espacios contiguos");
    break;
  case (op_noValida):
    printf("La operacion solicitada no es valida\n");
    break;
  case (com_desconocido):
    printf("El comando es desconocido\n");
    break;
  case (com_empezaAalfa):
    printf("Los comandos empiezan con una letra\n"); 
    break;
  case (com_faltaArg):
    printf("Faltan argumentos\n");
    break;
  case (com_sobranArg):
    printf("Sobran argumentos\n");
    break;
  default:
    printf("Error, error no traquedo: %d\n", buffer->estado);
    break;
  }
}

//Verificar que dos claves sean iguales
int iguadad_llaves(void *str1, void *str2) {
  return strcmp(str1, str2) == 0;
}

unsigned long int encoder(void *llave) {
  String str = (String) llave;
  unsigned long int salida = 0;
  int lango = strlen(str);
  int i = 0;
  for (i = 0; i < lango; i++)
    salida += str[i] * pow(31, lango - (i + 1));
  return salida;
}

void liberar_dato(void *dato) {
  if (dato)
    conjunto_destruir(dato);
}

void liberar_llave(void *llave) {
  string_destruir(llave);
}

/*Lanzar interprete*/
void interprete(int bandera) {
  //Informacion al usuario
  TablaHash almacen =
      tablahash_crear(256, encoder, iguadad_llaves, liberar_llave,
                      liberar_dato);
  Buffer buffer = crear_buffer();
  if (bandera)
    imprime_contenido("info.txt");
  while (buffer->estado != com_salir) {
    limpiar_buffer(buffer);
    if (bandera)
      printf(">>> ");
    completar_bufer(buffer);
    if (buffer->estado > 0)
      ejecutar_buffer(buffer, almacen, bandera); // almacen
    else if (buffer->estado < 0)
      ejecutar_errores(buffer);
  }
  limpiar_buffer(buffer);
  free(buffer);
  //liberar memoria de la tabala hash
  tablahash_destruir(almacen);
  //  printf("Final\n");
}
