#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "ejecutar.h"
#include "buffer.h"
#include "cadenas.h"
#include "tablahash.h"
#include "conjunto.h"

#include "intervalo.h"

/*Extencion*/
void ejecutar_agregar_conjunto(TablaHash tablaHash, Buffer buffer) {
  //printf("%d\n", ((Intervalo) buffer->protoconjunto->primero->dato)->inicial);
  tablahash_agregar(tablaHash,
                    buffer->conjunto1,
                    conjunto_inicializar(buffer->protoconjunto));
  //printf("%s\n",buffer->conjunto1);
  buffer->conjunto1 = NULL;
  buffer->protoconjunto = NULL;
}

/*complemento*/
void ejecutar_complemento(TablaHash tablaHash, Buffer buffer) {
  //Busca el conjunto al cual se le busca el complemento
  Conjunto complemento = tablahash_buscar(tablaHash, buffer->conjunto2);
  //Si esta definido ese conjunto
  if (complemento) {
    tablahash_agregar(tablaHash,
                      buffer->conjunto1, conjunto_complemento(complemento));
    buffer->conjunto1 = NULL;
    //string_destruir(buffer->conjunto2);
    //buffer->conjunto2 = NULL;
  } else
    printf("No existe referencia a algun conjunto\n");
}

/*operaciones*/
void ejecutar_operacion(TablaHash tablaHash, Buffer buffer, Operacion funcion) {
  Conjunto conjunto2 = tablahash_buscar(tablaHash, buffer->conjunto2);
  Conjunto conjunto3 = tablahash_buscar(tablaHash, buffer->conjunto3);
  if (conjunto2 && conjunto3) {
    tablahash_agregar(tablaHash,
                      buffer->conjunto1, funcion(conjunto2, conjunto3));
    buffer->conjunto1 = NULL;
  } else
    printf("No existe referencia a algun conjunto\n");
}

/*imprimir*/
void ejecutar_imprimir(TablaHash tablaHash, Buffer buffer, int bandera) {
  Conjunto conjunto = tablahash_buscar(tablaHash, buffer->conjunto1);
  if (conjunto) {
    if (bandera)
      printf("%s = {", buffer->conjunto1);
    /*if (conjunto_vacio(conjunto))
      printf("{}\n");
    else
      conjunto_imprimir(conjunto);*/
    if (!conjunto_vacio(conjunto))
      conjunto_imprimir(conjunto);
    if (bandera)
      printf("}");
    printf("\n");
  } else
    printf("Este conjunto no esta definido\n");
}
