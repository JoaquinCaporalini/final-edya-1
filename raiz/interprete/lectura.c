/*Lib standar*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <ctype.h>

/*Para la generacion de conjuntos*/
#include "gslista.h"
#include "intervalo.h"
/*manipulacion de la entrada*/
#include "cadenas.h"
#include "buffer.h"
/*funciones de la entrada*/
#include "lectura.h"

void destruccion(void *dato) {
  intervalo_destruir(dato);
}

//imprimir ALIAS
int lectura_imprimir(Buffer buffer, unsigned int *espacios, String str,
                     unsigned int largo) {
  if (strncmp(str, "imprimir", 8) == 0) {
    if (9 < largo) {
      buffer->estado = com_imprimir;
      buffer->conjunto1 = string_crear(largo - espacios[0]);
      strcpy(buffer->conjunto1, str + 9);
    } else
      buffer->estado = -com_faltaArg;   //Faltan argumentos
  } else
    buffer->estado = -com_desconocido;  //Comando desconocido
  return buffer->estado != 0;   //Estado de imprimir
}

//ALIAS = ~ALIAS
int lectura_complemento(Buffer buffer, unsigned int *espacios, String str,
                        unsigned int largo) {
  char c, op;
  buffer->conjunto1 = string_crear(espacios[0] + 1);
  if (sscanf(str, "%s = %c%c", buffer->conjunto1, &op, &c) == 3) {
    if (op == '~') {
      if (isalpha(c)) {
        buffer->conjunto2 = string_crear(largo - espacios[2]);  //ALIAS = ~alias //14 //7 14-7-1= 6
        strcpy(buffer->conjunto2, str + espacios[1] + 2);
        buffer->estado = op_complemento;        //Complemento
      } else
        buffer->estado = com_empezaAalfa;       //Debe deser alfa
    }
  }
  if (buffer->estado <= 0) {
    string_destruir(buffer->conjunto1);
    buffer->conjunto1 = NULL;
    if (buffer->conjunto2){
      string_destruir(buffer->conjunto2);
      buffer->conjunto2 = NULL;
    }
  }

  return buffer->estado != 0;
}

/*Reconocimiento de operacion*/
int reconocer_operacion(char op) {
  switch (op) {
  case ('|'):
    return op_union;            //Union
  case ('&'):
    return op_interseccion;     //interseccion
  case ('-'):
    return op_resta;            //resta
  default:
    return -1;                  // desconocido
  }
}

//ALIAS = ALIAS # ALIAS
int lectura_operaciones(Buffer buffer, unsigned int *espacios, String str,
                        unsigned int largo) {
  buffer->conjunto1 = string_crear(espacios[0] + 1);
  buffer->conjunto2 = string_crear(espacios[2] - espacios[1]);
  char c, op;
  if (sscanf
      (str, "%s = %s %c %c", buffer->conjunto1, buffer->conjunto2, &op,
       &c) == 4) {
    if (espacios[3] - espacios[2] == 2) {
      buffer->estado = reconocer_operacion(op);
      if (buffer->estado > 0) {
        buffer->conjunto3 = string_crear(largo - espacios[3]);
        strcpy(buffer->conjunto3, str + espacios[3] + 1);
      }
    } else
      buffer->estado = -op_noValida;    //Esstado invalido, op no valida.
  }

  if (buffer->estado <= 0) {
    memoria_buffer(buffer);
  }

  return buffer->estado != 0;
}

//ALIAS = {} //10 -7 -1;
//ALIAS = {a,b,c,d,e,f,g,h,i,j,k,l,...,z}
//39     7                                32
int lectura_extencion(Buffer buffer, unsigned int *espacios, String str,
                      unsigned int largo) {
  buffer->conjunto1 = string_crear(espacios[0] + 1);
  if ((largo - espacios[1] - 1) == 2
      && strcmp(str + espacios[1] + 1, "{}") == 0) {
    sscanf(str, "%s", buffer->conjunto1);
    buffer->estado = def_extencion;     //Va
    buffer->protoconjunto = gslist_crear();
    return 1;
  }
  String prosesado = string_crear(largo - espacios[1]);
  if (str[largo - 1] == '}'
      && sscanf(str, "%s = {%s", buffer->conjunto1, prosesado) == 2) {
    char coma = ',';
    long int numero;
    int salida = 1, verificador;
    buffer->protoconjunto = gslist_crear();
    verificador = sscanf(prosesado, "%ld%c%s", &numero, &coma, prosesado);
    while (coma == ',' && verificador == 3 && salida) {
      if (!(numero < INT_MIN) && !(INT_MAX < numero)) {
        gslist_agregar_final(buffer->protoconjunto,
                             intervalo_crear(numero, numero));
        verificador = sscanf(prosesado, "%ld%c%s", &numero, &coma, prosesado);
      } else
        salida = 0;
    }
    if (verificador == 2 && coma == '}' && salida) {
      gslist_agregar_final(buffer->protoconjunto,
                           intervalo_crear(numero, numero));
      buffer->estado = def_extencion;   //Conjunto cread0
    } else {
      buffer->estado = -com_desconocido;        //No hay patron
    }
  } else {
    buffer->estado = -com_desconocido;      //No hay patron
    free(buffer->conjunto1);
    buffer->conjunto1 = NULL;
  }
  free(prosesado);
  return buffer->estado != 0;
}

/*ALIAS = {x : m <= x <= n}*/
//     5 7 10 12 14 17 19 22
int lectura_comprension(Buffer buffer, unsigned int *espacios, String str) {
  char c, var1, var2;
  long int m, n;
  buffer->conjunto1 = string_crear(espacios[0] + 1);
  int validar = sscanf(str,
                       "%s = {%c : %ld <= %c <= %ld}%c",
                       buffer->conjunto1, &var1, &m, &var2, &n, &c);
  if (validar == 5 && m <= n && var1 == var2) {
    buffer->estado = def_comprension;   //comprension
    buffer->protoconjunto = gslist_crear();
    gslist_agregar_final(buffer->protoconjunto, intervalo_crear(m, n));
  } else {
    free(buffer->conjunto1);
    buffer->conjunto1 = NULL;
    if (validar != 5)
      buffer->estado = -com_desconocido;        //Problema con el patron
    else if (n < m)
      buffer->estado = -def_intervalo;  //intervalo no valido
    else
      buffer->estado = -def_variables;  //Problemas con las variables
  }
  return buffer->estado != 0;
}
