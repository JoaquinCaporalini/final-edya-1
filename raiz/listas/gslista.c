#include "gslista.h"
#include <stdlib.h>
#include <stdio.h>

//Esta funcion es necesaria principalmente internamente. 
GSNodo *gsnodo_crear(void *dato) {
  //pedir memoria para el nodo de la estructura
  GSNodo *nodo = malloc(sizeof(GSNodo));
  nodo->dato = dato;
  nodo->sig = NULL;
  return nodo;
}

//Crea la estructura base, en la cual alojar a la lista.
GSList gslist_crear() {
  GSList lista = malloc(sizeof(struct _SGList));
  lista->ultimo = lista->primero = NULL;
  return lista;
}

//nos indica si la lista esta vacia
//(NO tiene elementos)
int gslist_vacia(GSList lista) {
  return (!lista->primero && !lista->ultimo);
}

//
int gsnodo_vacio(GSNodo * nodo) {
  return (!nodo);
}

//Vacia la lista, pero no elimina la estructura
void gslist_vaciar(GSList list, FuncionLiberadora liberar) {
  GSNodo *nodo = list->primero;
  while (!gsnodo_vacio(nodo)) {
    list->primero = list->primero->sig;
    liberar(nodo->dato);
    free(nodo);
    nodo = list->primero;
  }
}

//Libera la estructura completa
void gslist_destruir(GSList list, FuncionLiberadora liberar) {
  gslist_vaciar(list, liberar);
  free(list);
}

//Dada una GSLista se agrega un nueo dato al final
void gslist_agregar_final(GSList list, void *dato) {
  //Si no hay ningun dato el inicio y el final son el mismo
  if (gslist_vacia(list)) {
    list->ultimo = list->primero = gsnodo_crear(dato);
  } else {
    list->ultimo->sig = gsnodo_crear(dato);
    list->ultimo = list->ultimo->sig;
  }
}

//Dada una GSLista se agrega un nueo dato al inicio
void gslist_agregar_inicio(GSList list, void *dato) {
  //Si no hay ningun dato el inicio y el final son el mismo
  if (gslist_vacia(list)) {
    list->ultimo = list->primero = gsnodo_crear(dato);
  } else {
    GSNodo *nodo = gsnodo_crear(dato);
    nodo->sig = list->primero;
    list->primero = nodo;
  }
}

//Esta funcion esta pensada para imprimir los datos dentro de la lista
void gslist_recorrer(GSList list, FuncionRecorre recorrer) {
  GSNodo *nodo = list->primero;
  while (!gsnodo_vacio(nodo)) {
    recorrer(nodo->dato);
    nodo = nodo->sig;
    if (nodo)
      printf(", ");
  }
}
