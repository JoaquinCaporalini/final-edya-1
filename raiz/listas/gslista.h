#ifndef __GSLIST_H__
#define __GSLIST_H__

#include <stddef.h>

typedef struct _GSNodo {
  void *dato;
  struct _GSNodo *sig;
} GSNodo;

typedef struct _SGList {
  GSNodo *ultimo;
  GSNodo *primero;
} *GSList;

/*Libera el dato del nodo de la lista*/
typedef void (*FuncionLiberadora) (void *);

/*Funcion capaz de realizar operaciones sobre el dato de la lista*/
typedef void (*FuncionRecorre) (void *);

/*Genera un nodo de la lista*/
GSNodo *gsnodo_crear(void *dato);

/*Realiza las peticiones de memoria y genera la estructura*/
GSList gslist_crear();

/*Nos indica si la lista NO tiene elementos*/
int gslist_vacia(GSList list);

/*Nos dice si un nodo NO tiene un dato*/
int gsnodo_vacio(GSNodo * nodo);

/*Libera los datos de la lista*/
void gslist_vaciar(GSList list, FuncionLiberadora liberar);

/*Libera los datos de la lista y la destrulle*/
void gslist_destruir(GSList list, FuncionLiberadora liberar);

/*Se agrega un dato al final*/
void gslist_agregar_final(GSList list, void *dato);

/*Se agrega un dato al inicio*/
void gslist_agregar_inicio(GSList list, void *dato);

/*Se aplica una funcion a cada elemento de la lista*/
void gslist_recorrer(GSList list, FuncionRecorre recorrer);

#endif                          /* __GSLIST_H__ */
