#include <stdio.h>
#include <stdlib.h>

#include "tablahash.h"
//#include "cola.h"
#include "gslista.h"

TablaHash tablahash_crear(unsigned int tamanio, FuncionHash funcionHash,
                          IgualdadHash iguadadHash, FuncionLiberar libLlave,
                          FuncionLiberar libDato) {
  TablaHash tablaHash = malloc(sizeof(struct _TablaHash));
  tablaHash->tamanio = tamanio;
  tablaHash->tabla = calloc(tamanio, sizeof(GSList));
  tablaHash->funcionHash = funcionHash;
  tablaHash->igualdadHash = iguadadHash;
  tablaHash->libLlave = libLlave;
  tablaHash->libDato = libDato;
  return tablaHash;
}

/*Dada una llave se busca el dato que le corresponda*/
THNodo tablahash_buscar_par(TablaHash tablaHash, void *llave) {
  unsigned long int numHash = (tablaHash->funcionHash) (llave);
  int condSalida = 1;
  if (!tablaHash->tabla[numHash % tablaHash->tamanio] ||
      gslist_vacia(tablaHash->tabla[numHash % tablaHash->tamanio]))
    return NULL;
  GSNodo *nodo = ((tablaHash->tabla)[numHash % tablaHash->tamanio])->primero;
  THNodo dato;
  while (condSalida && nodo) {
    dato = ((THNodo) nodo->dato);
    if (dato->hash == numHash && tablaHash->igualdadHash(dato->llave, llave))
      condSalida = 0;
    nodo = nodo->sig;
  }
  if (!condSalida) {
    return dato;                //printf("encontrado\n");
  }
  return NULL;
}

void tablahash_insertar_nodo(TablaHash tablaHash, THNodo nodo) {
  unsigned int claveHash = nodo->hash % tablaHash->tamanio;
  if (!tablaHash->tabla[claveHash])
    tablaHash->tabla[claveHash] = gslist_crear();
  gslist_agregar_inicio((tablaHash->tabla)[claveHash], nodo);
}

void tablahash_agregar(TablaHash tablaHash, void *llave, void *dato) {
  unsigned int claveHash =
      (tablaHash->funcionHash) (llave) % tablaHash->tamanio;
  //printf("Clave : %u\n", claveHash);
  if (!tablaHash->tabla[claveHash])
    tablaHash->tabla[claveHash] = gslist_crear();
  //Busco si la llave ya fue colocada en la tabla hash.
  THNodo existente = tablahash_buscar_par(tablaHash, llave);
  //Si ya fue colocada se actualiza el dato.
  if (existente) {
    //Se libera el dato viejo
    (tablaHash->libDato) (existente->dato);
    //se agrega el dato nuevo;
    existente->dato = dato;
    //Se libera el espacio de la llave que se paso.
    (tablaHash->libLlave) (llave);
  } else {
    existente = malloc(sizeof(struct _THNodo));
    existente->dato = dato;
    existente->llave = llave;
    //printf ("%s\n", (char *) llave);
    existente->hash = tablaHash->funcionHash(llave);
    tablahash_insertar_nodo(tablaHash, existente);
    //gslist_agregar_inicio(tablaHash->tabla[claveHash],existente);
  }
}

void *tablahash_buscar(TablaHash tablaHash, void *llave) {
  THNodo nodo = tablahash_buscar_par(tablaHash, llave);
  return (nodo) ? nodo->dato : NULL;
}

/* Proceso de destruir esta estructura */
void nodo_hash_lib(void *hashNode) {
  free((THNodo) hashNode);
}

void liberar_elemento(THNodo nodo, FuncionLiberar llave, FuncionLiberar dato) {
  if (nodo) {
    //printf("Nivel 4");
    dato(nodo->dato);
    llave(nodo->llave);
    //printf("Nivel 3");
    nodo_hash_lib(nodo);
  }
}

void liberar_listas(GSList lista, FuncionLiberar llave, FuncionLiberar dato) {
  if (lista) {
    GSNodo *nodo = lista->primero;
    while (!gsnodo_vacio(nodo)) {
      lista->primero = lista->primero->sig;
      liberar_elemento(nodo->dato, llave, dato);
      free(nodo);
      nodo = lista->primero;
    }
    //printf("Nivel 2");
    free(lista);
  }
}

/*Se libera el espacio de la tabla*/
void tablahash_destruir(TablaHash tablaHash) {
  for (unsigned int i = 0; i < tablaHash->tamanio; i++) {
    //printf("%u ", i);
    liberar_listas(tablaHash->tabla[i], tablaHash->libLlave,
                   tablaHash->libDato);
  }
  //nivel 1
  free(tablaHash->tabla);
  //printf("Nivel 1\n");
  free(tablaHash);
  //printf("Nivel 0\n");
}
