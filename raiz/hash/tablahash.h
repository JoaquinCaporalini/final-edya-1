#ifndef __TABLAHASH_H__
#define __TABLAHASH_H__

#include <stddef.h>

#include "gslista.h"

/*Puntero a funcion que genera en valor del hash*/
typedef unsigned long int (*FuncionHash) (void *llave);

/*Igualdad de llaves*/
typedef int (*IgualdadHash) (void *llave1, void *llave2);

/*Funcion que destruye una llave o un dato*/
typedef void (*FuncionLiberar) (void *nodo);

typedef struct _THNodo {
  unsigned long int hash;
  void *llave;
  void *dato;
} *THNodo;

typedef struct _TablaHash {
  unsigned int tamanio;
  //unsigned int ocupacion;
  GSList *tabla;
  FuncionHash funcionHash;
  IgualdadHash igualdadHash;
  FuncionLiberar libLlave;
  FuncionLiberar libDato;
} *TablaHash;

/*Se crea una tabla hash a partir de un tamanio inicial*/
TablaHash tablahash_crear(unsigned int tamanio, FuncionHash funcionHash,
                          IgualdadHash iguadadHash, FuncionLiberar libLlave,
                          FuncionLiberar libDato);

/*Se agrega una llave|dato a la table hash*/
void tablahash_agregar(TablaHash tablaHash, void *llave, void *dato);

/*Dada una llave se busca el dato que le corresponda*/
void *tablahash_buscar(TablaHash tablaHash, void *llave);

/*Se evita la alta taza de ocupacion*/
void *tablahash_rehash(TablaHash tablaHash);

/*Se libera el espacio de la tabla*/
void tablahash_destruir(TablaHash tablaHash);

#endif                          /* __TABLAHASH_H__ */
